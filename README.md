# P2PChat
This is a very simple p2p chat written with java. 
***
To start using this project you need to connect the library **javax.json-1.0.2.jar** in the project.
In Intellij IDEA: **File -> Project Structure -> Libraries**, click **+** and add this library file.
***
Read more about **p2p** here: <https://en.wikipedia.org/wiki/Peer-to-peer>

